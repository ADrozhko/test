package com.test.controller;

import com.test.service.ILinkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;


@Controller
@RequestMapping("/link")
public class LinkController {

    @Autowired
    ILinkService linkService;

    @RequestMapping(value = "/progress", method = RequestMethod.GET)
    @ResponseBody
    public String getProgress(Integer opdsId) {
        return linkService.getOPDSLinkCount(opdsId, true) + "/" + linkService.getOPDSLinkCount(opdsId, null);
    }
}

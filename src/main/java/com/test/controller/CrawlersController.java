package com.test.controller;

import com.test.managers.OPDSCrawlerManager;
import com.test.service.IOPDSService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;


@Controller
@RequestMapping(value = "/crawlers")
public class CrawlersController {

    @Autowired
    OPDSCrawlerManager opdsCrawlerManager;

    @Autowired
    IOPDSService opdsService;

    @RequestMapping(value = "")
    public ModelAndView getCrawlerPage(ModelMap map) {
        ModelAndView mav = new ModelAndView("crawlers");
        mav.addObject("opdsList", opdsService.getAllOPDS());
        return mav;
    }

    @RequestMapping("/{opdsId}/{action}")
    @ResponseBody
    public void action(@PathVariable Integer opdsId, @PathVariable String action) {
        opdsCrawlerManager.crawlerAction(opdsId, action);
    }
}

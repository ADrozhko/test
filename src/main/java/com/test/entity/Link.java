package com.test.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;

@Entity
@Table(name = "LINK")
public class Link {

    @Id
    @GeneratedValue
    @Column(name = "ID")
    private Integer id;

    @Column(name = "URL")
    private String url;

    @Column(name = "CHECKED")
    private Boolean checked = false;

    @Column(name = "ERROR")
    private Boolean error = false;

    @ManyToOne
    @JoinTable(
            name = "OPDS_LINK",
            joinColumns = {@JoinColumn(name = "LINK_ID")},
            inverseJoinColumns = {@JoinColumn(name = "OPDS_ID")}
    )
    private OPDS opds;

    public Integer getId() {
        return id;
    }

    public Link(String url) {
        this.url = url;
    }
    public Link(OPDS opds) {
        this.opds = opds;
    }
    public Link(OPDS opds, String url) {
        this.opds = opds;
        this.url = url;
    }

    public Link() {

    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Boolean getChecked() {
        return checked;
    }

    public void setChecked(Boolean checked) {
        this.checked = checked;
    }

    public OPDS getOpds() {
        return opds;
    }

    public void setOpds(OPDS opds) {
        this.opds = opds;
    }

    public Boolean getError() {
        return error;
    }

    public void setError(Boolean error) {
        this.error = error;
    }
}

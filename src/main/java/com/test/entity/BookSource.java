package com.test.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "BOOK_SOURCE")
public class BookSource {

    @Id
    @GeneratedValue
    @Column(name = "ID")
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "BOOK_ID")
    private Book book;

    @ManyToOne
    @JoinColumn(name = "OPDS_ID")
    private OPDS opds;

    @Column(name = "URL")
    private String url;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinTable(
            name = "BOOK_SOURCE_TYPE",
            joinColumns = {@JoinColumn(name = "BOOK_SOURCE_ID")},
            inverseJoinColumns = {@JoinColumn(name = "TYPE_ID")}
    )
    private Type type;

    public BookSource() {}

    public BookSource(String url, Type type, OPDS opds) {
        this.url = url;
        this.type = type;
        this.opds = opds;
    }
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public OPDS getOpds() {
        return opds;
    }

    public void setOpds(OPDS opds) {
        this.opds = opds;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }
}

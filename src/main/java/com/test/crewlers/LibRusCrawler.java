package com.test.crewlers;


import com.test.bean.PatternsBean;
import com.test.entity.Book;
import com.test.entity.Link;
import com.test.util.UrlUtil;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class LibRusCrawler extends AbstractCrawler{
    @PostConstruct
    private void init() {
        setOpds(opdsService.getOPDSByID(3));
        setupPattern();
    }

    @Override
    public List<Link> getUrlsFromPage(String pageText, String url) {
        return UrlUtil.getLinksFromPage(pageText, url).stream().map(u -> new Link(getOpds(), u)).collect(Collectors.toList());
    }

    @Override
    public void setupPattern() {
        PatternsBean patternsBean = getPatterns();
        patternsBean.setLanguagePattern(Pattern.compile("<dcterms:language>(.*?)</dcterms:language>"));
        patternsBean.setPublishDatePattern(Pattern.compile("<dcterms:issued>(.*?)</dcterms:issued>"));
        patternsBean.setContentPattern(Pattern.compile("<summary.*?>(.*?)</summury>"));
        patternsBean.setBookSourcePattern(Pattern.compile("<link type=.application/.*?. href=.*? rel=.http://.*? />"));
    }

    @Override
    public List<Book> getBooksFromPage(String pageText) {
        Matcher entryMatcher = getPatterns().getBookEntryPattern().matcher(pageText);
        List<Book> books = new ArrayList<>();
        while (entryMatcher.find()) {
            String entryText = entryMatcher.group(0);
            Book book = UrlUtil.getBook(entryText, getOpds(), getPatterns());
            if(null != book) {
                books.add(book);
            }
        }
        return books;
    }
}

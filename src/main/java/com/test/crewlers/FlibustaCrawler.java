package com.test.crewlers;

import com.test.entity.Book;
import com.test.entity.Link;
import com.test.util.Const;
import com.test.util.UrlUtil;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.stream.Collectors;

@Component
public class FlibustaCrawler extends AbstractCrawler {

    @PostConstruct
    private void init() {
        setOpds(opdsService.getOPDSByID(1));
    }

    @Override
    public List<Link> getUrlsFromPage(String pageText, String url) {
        return UrlUtil.getLinksFromPage(pageText, url).stream().map(u -> new Link(getOpds(), u)).collect(Collectors.toList());
    }

    @Override
    public List<Book> getBooksFromPage(String pageText) {
        Matcher entryMatcher = getPatterns().getBookEntryPattern().matcher(pageText);
        List<Book> books = new ArrayList<>();
        while (entryMatcher.find()) {
            String entryText = entryMatcher.group(0);
            if (entryText.contains("tag:book")) {
                Book book = UrlUtil.getBook(entryText, getOpds(), getPatterns());
                if(null != book) {
                    books.add(book);
                }
            }
        }
        return books;
    }

    @Override
    public void setupPattern() {

    }
}

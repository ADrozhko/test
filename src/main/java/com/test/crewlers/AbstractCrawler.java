package com.test.crewlers;


import com.test.bean.PatternsBean;
import com.test.entity.Book;
import com.test.entity.Link;
import com.test.entity.OPDS;
import com.test.managers.BooksManager;
import com.test.managers.OPDSLinkManager;
import com.test.service.IOPDSService;
import com.test.util.UrlUtil;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.stream.Collectors;


public abstract class AbstractCrawler implements Runnable {

    @Autowired
    OPDSLinkManager opdsLinkManager;

    @Autowired
    IOPDSService opdsService;

    @Autowired
    BooksManager booksManager;

    static Logger logger = Logger.getLogger(AbstractCrawler.class);
    private OPDS opds;
    private Boolean stopped = false;
    private Boolean pause = false;
    private PatternsBean patterns = new PatternsBean();

    private String getTextFromPage(Link link) {
        return UrlUtil.getTextFromUrl(link);
    }

//    public List<Link> getUrlsFromPage(String pageText, String url) {
//        return UrlUtil.getLinksFromPage(pageText, url).stream().map(u -> new Link(opds, u)).collect(Collectors.toList());
//    }
//
//    public List<Book> getBooksFromPage(String pageText) {
//        return UrlUtil.getBooksFromPage(pageText, opds);
//    }
    public abstract List<Link> getUrlsFromPage(String pageText, String url);

    public abstract List<Book> getBooksFromPage(String pageText);

    public void parsePage() {
        Link link = opdsLinkManager.getLink(opds);
        Integer connectionCount = 1;
        while(null != link) {
            String pageText = getTextFromPage(link);
            if(null != pageText) {
                opdsLinkManager.addLinks(getUrlsFromPage(pageText, link.getUrl()));
                booksManager.addBooks(getBooksFromPage(pageText));
                opdsLinkManager.setLinkChecked(link, false);
            } else {
                connectionCount += 1;
            }
            if(connectionCount == 10) {
                opdsLinkManager.setLinkChecked(link, true);
                connectionCount = 1;
            }
            link = opdsLinkManager.getLink(opds);
            if(stopped) {
                logger.info("Thread : " + Thread.currentThread().getName() + " STOPPED");
                stopped = false;
                break;
            }
            while (pause) {
                if(stopped) {
                    logger.info("Thread : " + Thread.currentThread().getName() + " STOPPED");
                    stopped = false;
                    break;
                }
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void run(){
        Thread.currentThread().setName(this.getClass().getName());
        logger.info("Thread : " + Thread.currentThread().getName() + " STARTED");
        parsePage();
        logger.info("Thread : " + Thread.currentThread().getName() + " FINISHED");
    }

    public OPDS getOpds() {
        return opds;
    }

    public void setOpds(OPDS opds) {
        this.opds = opds;
    }

    public Boolean getPause() {
        return pause;
    }

    public void setPause(Boolean pause) {
        this.pause = pause;
    }

    public Boolean getStopped() {
        return stopped;
    }

    public void setStopped(Boolean stopped) {
        this.stopped = stopped;
    }

    public PatternsBean getPatterns() {
        return patterns;
    }

    public void setPatterns(PatternsBean patterns) {
        this.patterns = patterns;
    }

    public abstract void setupPattern();

}

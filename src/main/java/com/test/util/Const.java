package com.test.util;



import java.util.regex.Pattern;

public class Const {

    /*
        PATTERNS
     */

    public static Pattern LINK_TAG_PATTERN = Pattern.compile("<link.*? type=.*?;profile=opds-catalog.*?/>",
            Pattern.DOTALL);
    public static Pattern LINK_PATTERN = Pattern.compile("\\s*(?i)href\\s*=\\s*(\"([^\"]*\")|'[^']*'|([^'\">\\s]+))",
            Pattern.CASE_INSENSITIVE|Pattern.DOTALL);


    public static Pattern BOOK_ENTRY_PATTERN = Pattern.compile("<entry.*?>(.*?)</entry>", Pattern.MULTILINE|Pattern.DOTALL);
    public static Pattern TITLE_PATTERN = Pattern.compile("<title.*?>(.*?)</title>",
            Pattern.CASE_INSENSITIVE|Pattern.DOTALL);
    public static Pattern ENTRY_AUTHOR_NAME_PATTERN = Pattern.compile("<author> <name>(.*?)</name>", Pattern.MULTILINE|Pattern.DOTALL);
    public static Pattern ENTRY_LANGUAGE_PATTERN = Pattern.compile("<dc:language>(.*?)</dc:language>");
    public static Pattern ENTRY_PUBLISH_DATE_PATTERN = Pattern.compile("<dc:issued>(.*?)</dc:issued>");
    public static Pattern ENTRY_CATEGORY_PATTERN = Pattern.compile("<category term=(.*?) label.*?/>");
    public static Pattern ENTRY_CONTENT_PATTERN = Pattern.compile("<content.*?>(.*?)</content>");
    public static Pattern BOOK_SOURCE_PATTERN = Pattern.compile("<link href=.*? rel=.http://.*? type=.application/.*?. .*?/>", Pattern.MULTILINE|Pattern.DOTALL);
    public static Pattern BOOK_SOURCE_TYPE_PATTERN = Pattern.compile("type=.application/(.*?). .*?", Pattern.MULTILINE|Pattern.DOTALL);


}

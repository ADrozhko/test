package com.test.service;

import com.test.entity.BookSource;

import java.util.List;

public interface IBookSourceService {

    public void saveSources(List<BookSource> sourceList);
}

package com.test.service;


import com.test.entity.Book;

import java.util.List;

public interface IBookService {

    public void saveBook(Book book);

    public void deleteBook(Book book);

    public List<Book> getAllBooks();

    public Book getBookById(Integer id);

}

package com.test.service.impl;


import com.test.dao.IAuthorDao;
import com.test.entity.Author;
import com.test.service.IAuthorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AuthorServiceImpl implements IAuthorService{

    @Autowired
    IAuthorDao authorDao;

    @Override
    public void saveAuthor(Author author) {
        authorDao.saveAuthor(author);
    }

    @Override
    public void deleteAuthor(Author author) {
        authorDao.deleteAuthor(author);
    }

    @Override
    public List<Author> getAllAuthors() {
        return authorDao.getAllAuthors();
    }

    @Override
    public Author getAuthorById(Integer id) {
        return authorDao.getAuthorById(id);
    }
}

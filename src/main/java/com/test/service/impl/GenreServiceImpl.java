package com.test.service.impl;

import com.test.dao.IGenreDao;
import com.test.entity.Genre;
import com.test.service.IGenreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GenreServiceImpl implements IGenreService{

    @Autowired
    IGenreDao genreDao;

    @Override
    public void saveGenre(Genre genre) {
        genreDao.saveGenre(genre);
    }

    @Override
    public void deleteGenre(Genre genre) {
        genreDao.deleteGenre(genre);
    }

    @Override
    public List<Genre> getAllGenres() {
        return genreDao.getAllGenres();
    }

    @Override
    public Genre getGenreById(Integer id) {
        return genreDao.getGenreById(id);
    }
}

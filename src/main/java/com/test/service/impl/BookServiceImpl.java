package com.test.service.impl;

import com.test.dao.IBookDao;
import com.test.dao.ILanguageDao;
import com.test.entity.Book;
import com.test.service.IBookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookServiceImpl implements IBookService {

    @Autowired
    IBookDao bookDao;

    @Autowired
    ILanguageDao languageDao;

    @Override
    public void saveBook(Book book) {
        if(null == book.getId()) {
            bookDao.saveBook(book);

        } else {
            bookDao.updateBook(book);
        }
    }

    @Override
    public void deleteBook(Book book) {
        bookDao.deleteBook(book);
    }

    @Override
    public List<Book> getAllBooks() {
        return bookDao.getAllBooks();
    }

    @Override
    public Book getBookById(Integer id) {
        return bookDao.getBookById(id);
    }
}

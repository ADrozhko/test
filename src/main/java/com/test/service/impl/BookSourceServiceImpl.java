package com.test.service.impl;

import com.test.dao.IBookSourceDao;
import com.test.entity.BookSource;
import com.test.service.IBookSourceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookSourceServiceImpl implements IBookSourceService {

    @Autowired
    IBookSourceDao bookSourceDao;

    @Override
    public void saveSources(List<BookSource> sourceList) {
        sourceList.forEach(bookSourceDao::saveBookSource);
    }
}

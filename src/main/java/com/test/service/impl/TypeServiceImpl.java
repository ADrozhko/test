package com.test.service.impl;

import com.test.dao.ITypeDao;
import com.test.entity.Type;
import com.test.service.ITypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TypeServiceImpl implements ITypeService{

    @Autowired
    ITypeDao typeDao;

    @Override
    public void saveType(Type type) {
        typeDao.saveType(type);
    }

    @Override
    public void saveTypes(List<Type> types) {
        types.forEach(this::saveType);
    }

    @Override
    public void deleteType(Type type) {
        typeDao.deleteType(type);
    }

    @Override
    public List<Type> getAllTypes() {
        return typeDao.getAllTypes();
    }

    @Override
    public Type getTypeById(Integer id) {
        return typeDao.getTypeById(id);
    }
}

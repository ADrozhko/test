package com.test.service.impl;

import com.test.dao.IOPDSDao;
import com.test.entity.OPDS;
import com.test.service.IOPDSService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OPDSServiceImpl implements IOPDSService {

    @Autowired
    IOPDSDao opdsDao;
    @Override
    public OPDS getOPDSByID(int id) {
        return opdsDao.getOPDSByID(id);
    }

    @Override
    public List<OPDS> getAllOPDS() {
        return opdsDao.getAllOPDS();
    }
}

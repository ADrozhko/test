package com.test.service.impl;

import com.test.dao.ILanguageDao;
import com.test.entity.Language;
import com.test.service.ILanguageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LanguageSeviceImpl implements ILanguageService{

    @Autowired
    ILanguageDao languageDao;

    @Override
    public void saveLanguage(Language language) {
        languageDao.saveLanguage(language);
    }

    @Override
    public void deleteLanguage(Language language) {
        languageDao.deleteLanguage(language);
    }

    @Override
    public List<Language> getAllLanguages() {
        return languageDao.getAllLanguages();
    }

    @Override
    public Language getLanguageById(Integer id) {
        return languageDao.getLanguageById(id);
    }
}

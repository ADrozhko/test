package com.test.service.impl;

import com.test.dao.ILinkDao;
import com.test.entity.Link;
import com.test.service.ILinkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service(value = "linkService")
public class LinkServiceImpl implements ILinkService{

    @Autowired
    ILinkDao linkDao;

    @Override
    public List<Link> getAllLinks() {
        return linkDao.getAllLinks();
    }

    @Override
    public void saveLink(Link link) {
        if(null == link.getId()) {
            linkDao.saveLink(link);
        } else {
            linkDao.updateLink(link);
        }
    }

    @Override
    public Integer getOPDSLinkCount(int opdsId, Boolean checked) {
        return linkDao.getOPDSLinkCount(opdsId, checked);
    }

}

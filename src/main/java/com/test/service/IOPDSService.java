package com.test.service;


import com.test.entity.OPDS;

import java.util.List;

public interface IOPDSService {

    public OPDS getOPDSByID(int id);

    public List<OPDS> getAllOPDS();
}

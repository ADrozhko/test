package com.test.service;


import com.test.entity.Type;

import java.util.List;

public interface ITypeService {
    public void saveType(Type type);

    public void saveTypes(List<Type> types);

    public void deleteType(Type type);

    public List<Type> getAllTypes();

    public Type getTypeById(Integer id);
}

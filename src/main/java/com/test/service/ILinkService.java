package com.test.service;

import com.test.entity.Link;

import java.util.List;

public interface ILinkService {

    public List<Link> getAllLinks();

    public void saveLink(Link link);

    public Integer getOPDSLinkCount(int opdsId, Boolean checked);

}

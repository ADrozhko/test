package com.test.service;


import com.test.entity.Author;

import java.util.List;

public interface IAuthorService {

    public void saveAuthor(Author author);

    public void deleteAuthor(Author author);

    public List<Author> getAllAuthors();

    public Author getAuthorById(Integer id);

}

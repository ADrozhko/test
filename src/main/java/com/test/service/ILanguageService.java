package com.test.service;

import com.test.entity.Language;

import java.util.List;

public interface ILanguageService {

    public void saveLanguage(Language language);

    public void deleteLanguage(Language language);

    public List<Language> getAllLanguages();

    public Language getLanguageById(Integer id);
}

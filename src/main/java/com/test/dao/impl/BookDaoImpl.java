package com.test.dao.impl;

import com.test.dao.IBookDao;
import com.test.entity.Book;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public class BookDaoImpl implements IBookDao {

    @Autowired
    private SessionFactory sessionFactory;

    private Session getSession() {
        return sessionFactory.getCurrentSession();
    }

    @Override
    public void saveBook(Book book) {
        getSession().save(book);
    }

    @Override
    public void updateBook(Book book) {getSession().update(book);}

    @Override
    public void deleteBook(Book book) {
        getSession().delete(book);
    }

    @Override
    public List<Book> getAllBooks() {
        return getSession().createCriteria(Book.class).list();
    }

    @Override
    public Book getBookById(Integer id) {
        return (Book) getSession().createCriteria(Book.class).
                add(Restrictions.eq("id", id)).uniqueResult();
    }
}

package com.test.dao.impl;

import com.test.dao.ILanguageDao;
import com.test.entity.Language;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.ResourceBundle;

@Repository
@Transactional
public class LanguageDaoImpl implements ILanguageDao{

    @Autowired
    private SessionFactory sessionFactory;

    private Session getSession() {
        return sessionFactory.getCurrentSession();
    }

    @Override
    public void saveLanguage(Language language) {
        getSession().save(language);
    }

    @Override
    public void deleteLanguage(Language language) {
        getSession().delete(language);
    }

    @Override
    public List<Language> getAllLanguages() {
        return getSession().createCriteria(Language.class).list();
    }

    @Override
    public Language getLanguageById(Integer id) {
        return (Language) getSession().createCriteria(Language.class).add(Restrictions.eq("id", id));
    }
}

package com.test.dao.impl;

import com.test.dao.IAuthorDao;
import com.test.entity.Author;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public class AuthorDaoImpl implements IAuthorDao{

    @Autowired
    private SessionFactory sessionFactory;

    private Session getSession() {
        return sessionFactory.getCurrentSession();
    }

    @Override
    public void saveAuthor(Author author) {
        getSession().save(author);
    }

    @Override
    public void deleteAuthor(Author author) {
        getSession().delete(author);
    }

    @Override
    public List<Author> getAllAuthors() {
        return getSession().createCriteria(Author.class).list();
    }

    @Override
    public Author getAuthorById(Integer id) {
        return (Author) getSession().createCriteria(Author.class).
                add(Restrictions.eq("id", id)).uniqueResult();
    }
}

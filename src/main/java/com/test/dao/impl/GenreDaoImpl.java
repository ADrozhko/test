package com.test.dao.impl;

import com.test.dao.IGenreDao;
import com.test.entity.Genre;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public class GenreDaoImpl implements IGenreDao{

    @Autowired
    private SessionFactory sessionFactory;

    private Session getSession() {
        return sessionFactory.getCurrentSession();
    }

    @Override
    public void saveGenre(Genre genre) {
        getSession().save(genre);
    }

    @Override
    public void deleteGenre(Genre genre) {
        getSession().delete(genre);
    }

    @Override
    public List<Genre> getAllGenres() {
        return getSession().createCriteria(Genre.class).list();
    }

    @Override
    public Genre getGenreById(Integer id) {
        return (Genre) getSession().createCriteria(Genre.class).add(Restrictions.eq("id", id));
    }
}

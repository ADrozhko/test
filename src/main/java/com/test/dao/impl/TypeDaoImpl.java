package com.test.dao.impl;

import com.test.dao.ITypeDao;
import com.test.entity.Type;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public class TypeDaoImpl implements ITypeDao{

    @Autowired
    private SessionFactory sessionFactory;

    private Session getSession() {
        return sessionFactory.getCurrentSession();
    }

    @Override
    public void saveType(Type type) {
        getSession().save(type);
    }

    @Override
    public void deleteType(Type type) {
        deleteType(type);
    }

    @Override
    public List<Type> getAllTypes() {
        return getSession().createCriteria(Type.class).list();
    }

    @Override
    public Type getTypeById(Integer id) {
        return (Type) getSession().createCriteria(Type.class).add(Restrictions.eq("id", id));
    }
}

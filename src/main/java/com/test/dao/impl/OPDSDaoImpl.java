package com.test.dao.impl;

import com.test.dao.IOPDSDao;
import com.test.entity.OPDS;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public class OPDSDaoImpl implements IOPDSDao {

    @Autowired
    private SessionFactory sessionFactory;

    private Session getSession() {
        return sessionFactory.getCurrentSession();
    }

    @Override
    public OPDS getOPDSByID(int id) {
        return (OPDS) getSession().createCriteria(OPDS.class)
                .add(Restrictions.eq("id", id)).uniqueResult();
    }

    @Override
    public List<OPDS> getAllOPDS() {
        return getSession().createCriteria(OPDS.class).list();
    }
}

package com.test.dao.impl;

import com.test.dao.IBookSourceDao;
import com.test.entity.BookSource;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
@Transactional
public class BookSourceDaoImpl implements IBookSourceDao{

    @Autowired
    private SessionFactory sessionFactory;

    private Session getSession() {
        return sessionFactory.getCurrentSession();
    }

    @Override
    public void saveBookSource(BookSource bookSource) {
        getSession().save(bookSource);
    }
}

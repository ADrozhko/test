package com.test.dao.impl;

import com.test.dao.ILinkDao;
import com.test.entity.Link;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public class LinkDaoImpl implements ILinkDao{

    @Autowired
    private SessionFactory sessionFactory;

    private Session getSession() {
        return sessionFactory.getCurrentSession();
    }

    @Override
    public List<Link> getAllLinks() {
        return getSession().createCriteria(Link.class).list();
    }

    @Override
    public void saveLink(Link link) {
        getSession().save(link);
    }

    @Override
    public void updateLink(Link link) {
        getSession().update(link);
    }

    @Override
    public Integer getOPDSLinkCount(int opdsId, Boolean checked) {
        Criteria criteria = getSession().createCriteria(Link.class);
        if(null != checked) {
            criteria.add(Restrictions.eq("checked", checked));
        }
        criteria.add(Restrictions.eq("opds.id", opdsId));
        return ((Number) criteria.setProjection(Projections.rowCount()).uniqueResult()).intValue();
    }
}

package com.test.dao;


import com.test.entity.Genre;

import java.util.List;

public interface IGenreDao {
    public void saveGenre(Genre genre);

    public void deleteGenre(Genre genre);

    public List<Genre> getAllGenres();

    public Genre getGenreById(Integer id);
}

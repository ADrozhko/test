package com.test.dao;

import com.test.entity.Link;

import java.util.List;

public interface ILinkDao {

    public List<Link> getAllLinks();

    public void saveLink(Link link);

    public void updateLink(Link link);

    public Integer getOPDSLinkCount(int opdsId, Boolean checked);
}

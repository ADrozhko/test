package com.test.dao;

import com.test.entity.BookSource;

public interface IBookSourceDao {

    public void saveBookSource(BookSource bookSource);
}

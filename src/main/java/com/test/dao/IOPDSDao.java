package com.test.dao;


import com.test.entity.OPDS;

import java.util.List;

public interface IOPDSDao {

    public OPDS getOPDSByID(int id);

    public List<OPDS> getAllOPDS();
}

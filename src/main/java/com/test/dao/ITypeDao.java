package com.test.dao;


import com.test.entity.Type;

import java.util.List;

public interface ITypeDao {
    public void saveType(Type type);

    public void deleteType(Type type);

    public List<Type> getAllTypes();

    public Type getTypeById(Integer id);
}

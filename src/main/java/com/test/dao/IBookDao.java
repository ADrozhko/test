package com.test.dao;


import com.test.entity.Book;

import java.util.List;

public interface IBookDao {

    public void saveBook(Book book);

    public void updateBook(Book book);

    public void deleteBook(Book book);

    public List<Book> getAllBooks();

    public Book getBookById(Integer id);

}

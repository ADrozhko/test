package com.test.dao;

import com.test.entity.Language;

import java.util.List;

public interface ILanguageDao {

    public void saveLanguage(Language language);

    public void deleteLanguage(Language language);

    public List<Language> getAllLanguages();

    public Language getLanguageById(Integer id);
}

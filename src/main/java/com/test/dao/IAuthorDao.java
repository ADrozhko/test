package com.test.dao;


import com.test.entity.Author;

import java.util.List;

public interface IAuthorDao {

    public void saveAuthor(Author author);

    public void deleteAuthor(Author author);

    public List<Author> getAllAuthors();

    public Author getAuthorById(Integer id);
}

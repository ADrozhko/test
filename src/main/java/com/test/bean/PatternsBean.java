package com.test.bean;


import java.util.regex.Pattern;

public class PatternsBean {
    private Pattern linkPattern = Pattern.compile("\\s*(?i)href\\s*=\\s*(\"([^\"]*\")|'[^']*'|([^'\">\\s]+))",
            Pattern.CASE_INSENSITIVE|Pattern.DOTALL);
    private Pattern bookEntryPattern = Pattern.compile("<entry.*?>(.*?)</entry>", Pattern.MULTILINE|Pattern.DOTALL);
    private Pattern bookTitlePattern = Pattern.compile("<title.*?>(.*?)</title>", Pattern.CASE_INSENSITIVE|Pattern.DOTALL);
    private Pattern authorNamePattern = Pattern.compile("<author> <name>(.*?)</name>", Pattern.MULTILINE|Pattern.DOTALL);
    private Pattern languagePattern = Pattern.compile("<dc:language>(.*?)</dc:language>");
    private Pattern publishDatePattern = Pattern.compile("<dc:issued>(.*?)</dc:issued>");
    private Pattern genrePattern = Pattern.compile("<category term=(.*?) label.*?/>");
    private Pattern contentPattern = Pattern.compile("<content.*?>(.*?)</content>");
    private Pattern bookSourcePattern = Pattern.compile("<link href=.*? rel=.http://.*? type=.application/.*?. .*?/>", Pattern.MULTILINE|Pattern.DOTALL);
    private Pattern bookSourceTypePattern = Pattern.compile("type=.application/(.*?). .*?", Pattern.MULTILINE|Pattern.DOTALL);

    public Pattern getBookEntryPattern() {
        return bookEntryPattern;
    }

    public void setBookEntryPattern(Pattern bookEntryPattern) {
        this.bookEntryPattern = bookEntryPattern;
    }

    public Pattern getBookTitlePattern() {
        return bookTitlePattern;
    }

    public void setBookTitlePattern(Pattern bookTitlePattern) {
        this.bookTitlePattern = bookTitlePattern;
    }

    public Pattern getAuthorNamePattern() {
        return authorNamePattern;
    }

    public void setAuthorNamePattern(Pattern authorNamePattern) {
        this.authorNamePattern = authorNamePattern;
    }

    public Pattern getLanguagePattern() {
        return languagePattern;
    }

    public void setLanguagePattern(Pattern languagePattern) {
        this.languagePattern = languagePattern;
    }

    public Pattern getPublishDatePattern() {
        return publishDatePattern;
    }

    public void setPublishDatePattern(Pattern publishDatePattern) {
        this.publishDatePattern = publishDatePattern;
    }

    public Pattern getGenrePattern() {
        return genrePattern;
    }

    public void setGenrePattern(Pattern genrePattern) {
        this.genrePattern = genrePattern;
    }

    public Pattern getContentPattern() {
        return contentPattern;
    }

    public void setContentPattern(Pattern contentPattern) {
        this.contentPattern = contentPattern;
    }

    public Pattern getBookSourcePattern() {
        return bookSourcePattern;
    }

    public void setBookSourcePattern(Pattern bookSourcePattern) {
        this.bookSourcePattern = bookSourcePattern;
    }

    public Pattern getBookSourceTypePattern() {
        return bookSourceTypePattern;
    }

    public void setBookSourceTypePattern(Pattern bookSourceTypePattern) {
        this.bookSourceTypePattern = bookSourceTypePattern;
    }

    public Pattern getLinkPattern() {
        return linkPattern;
    }

    public void setLinkPattern(Pattern linkPattern) {
        this.linkPattern = linkPattern;
    }
}

package com.test.managers;

import com.test.entity.Link;
import com.test.entity.OPDS;
import com.test.service.ILinkService;
import com.test.service.IOPDSService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Configurable
public class OPDSLinkManager {

    @Autowired
    ILinkService linkService;

    @Autowired
    IOPDSService opdsService;

    List<Link> links = new ArrayList<>();

    @PostConstruct
    public void init() {
        links = linkService.getAllLinks();
    }

    private Boolean isLinkPresent(Link link) {
        Optional<Link> result = links.stream().filter(l -> l.getUrl().equals(link.getUrl())).findFirst();
        return !result.isPresent();
    }

    public synchronized void addLinks(List<Link> links) {
        links.stream().filter(this::isLinkPresent).forEach(this::addLink);
    }

    private void addLink(Link link) {
        links.add(link);
        linkService.saveLink(link);
    }

    public synchronized Link getLink(OPDS opds) {

        List<Link> opdsLink = links.stream().filter(l -> l.getOpds().getId().equals(opds.getId())).collect(Collectors.toList());
        if(opdsLink.isEmpty()) {
            Link link = new Link(opds, opds.getUrl());
            addLink(link);
        }
        Optional<Link> link = links.stream().filter(l -> !l.getChecked() && !l.getError() && l.getOpds().getId().equals(opds.getId())).findFirst();
        if(link.isPresent()) {
            return link.get();
        } else {
            return null;
        }
    }


    public synchronized void setLinkChecked(Link link, Boolean error) {
        Link result = links.stream().filter(l -> l.getUrl().equals(link.getUrl())).findFirst().get();
        result.setChecked(true);
        result.setError(error);
        linkService.saveLink(result);
    }

}

package com.test.managers;

import com.test.crewlers.AbstractCrawler;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PreDestroy;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Component
public class OPDSCrawlerManager {

    ExecutorService executorService = Executors.newCachedThreadPool();
    static Logger logger = Logger.getLogger(AbstractCrawler.class);
    @Autowired
    List<AbstractCrawler> crawlers;

    @PreDestroy
    public void cleanUp() {
        crawlers.stream().forEach(crawler -> crawler.setStopped(true));
        try {
            wait(6000);
        } catch (InterruptedException e) {
            executorService.shutdown();
            e.printStackTrace();
        }
        executorService.shutdown();
        logger.info("All threads shutdown successfully!");
    }

    public void crawlerAction(int opdsId, String action) {
        Optional<AbstractCrawler> result = crawlers.stream().filter(crawler -> crawler.getOpds().getId().equals(opdsId)).findFirst();
        if (result.isPresent()) {
            final AbstractCrawler crawler = result.get();
            switch (action) {
                case "run":
                    executorService.execute(crawler::run);
                    break;
                case "stop":
                    crawler.setStopped(true);
                    break;
                case "pause":
                    crawler.setPause(!crawler.getPause());
                    break;
            }
        }
    }

}


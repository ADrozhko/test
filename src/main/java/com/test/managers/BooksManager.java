package com.test.managers;

import com.test.entity.Author;
import com.test.entity.Book;
import com.test.entity.BookSource;
import com.test.entity.Genre;
import com.test.entity.Language;
import com.test.entity.Type;
import com.test.service.IAuthorService;
import com.test.service.IBookService;
import com.test.service.IBookSourceService;
import com.test.service.IGenreService;
import com.test.service.ILanguageService;
import com.test.service.ITypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class BooksManager {

    @Autowired
    IBookService bookService;

    @Autowired
    ILanguageService languageService;

    @Autowired
    IAuthorService authorService;

    @Autowired
    IGenreService genreService;

    @Autowired
    ITypeService typeService;

    @Autowired
    IBookSourceService bookSourceService;

    private List<Book> books;
    private List<Language> languages;
    private List<Author> authors;
    private List<Genre> genres;
    private List<Type> types;

    @PostConstruct
    public void init() {
        books = bookService.getAllBooks();
        languages = languageService.getAllLanguages();
        authors = authorService.getAllAuthors();
        genres = genreService.getAllGenres();
        types = typeService.getAllTypes();
    }

    private Boolean isBookPresent(Book book) {
        Optional<Book> result = books.stream().filter(b -> b.getTitle().equals(book.getTitle())).findFirst();
        return result.isPresent();
    }

    private Boolean isSourcePresent(Book book, BookSource bookSource) {
        Optional<BookSource> result = book.getSources().stream().filter(s -> s.getUrl().equals(bookSource.getUrl())).findFirst();
        return result.isPresent();
    }

    private void addBookSources(Book book, List<BookSource> sources) {
        if(sources.isEmpty()) {
            return;
        }
        List<Type> bookSourceTypes = sources.stream().map(BookSource::getType).collect(Collectors.toList());
        bookSourceTypes.stream().forEach(type -> {
            if(!isTypePresent(type)) {
                typeService.saveType(type);
                types.add(type);
            } else {
                bookSourceTypes.get(bookSourceTypes.indexOf(type)).setId(types.stream().filter(g -> g.getName().equals(type.getName())).findFirst().get().getId());
            }
        });
        sources.forEach(s -> s.setBook(book));
        bookSourceService.saveSources(sources);
        List<BookSource> addedSources = book.getSources();
        addedSources.addAll(sources);
    }

    private void addSource(Book book) {
        Optional<Book> result = books.stream().filter(b -> b.getTitle().equals(book.getTitle())).findFirst();
        List<BookSource> addingSources = book.getSources().stream().filter(source -> !isSourcePresent(result.get(), source)).collect(Collectors.toList());
        addBookSources(result.get(), addingSources);
        bookService.saveBook(result.get());
    }

    public synchronized void addBooks(List<Book> books) {
        books.forEach(b -> {
            if(isBookPresent(b)) {
                addSource(b);
            } else {
                addBook(b);
            }
        });
    }

    private Boolean isLanguagePresent(Language language) {
        return languages.stream().filter(l -> l.getName().equals(language.getName())).findFirst().isPresent();
    }

    private Boolean isAuthorPresent(Author author) {
        return authors.stream().filter(a -> a.getName().equals(author.getName())).findFirst().isPresent();
    }

    private Boolean isGanrePresent(Genre genre) {
        return genres.stream().filter(g -> g.getName().equals(genre.getName())).findFirst().isPresent();
    }

    private Boolean isTypePresent(Type type) {
        return types.stream().filter(t -> t.getName().equals(type.getName())).findFirst().isPresent();
    }

    private void addBook(Book book) {
        books.add(book);
        saveDependency(book);
        bookService.saveBook(book);
        addBookSources(book, book.getSources());
    }
    private void saveDependency(Book book) {
        Language language = book.getLanguage();
        if(!isLanguagePresent(language)) {
            languages.add(language);
            languageService.saveLanguage(language);
        } else {
            book.setLanguage(languages.stream().filter(l -> l.getName().equals(language.getName())).findFirst().get());
        }
        List<Author> bookAuthors = book.getAuthors();
        bookAuthors.stream().forEach(author -> {
            if(!isAuthorPresent(author)) {
                authorService.saveAuthor(author);
                authors.add(author);
            } else {
                bookAuthors.get(bookAuthors.indexOf(author)).setId(authors.stream().filter(a -> a.getName().equals(author.getName())).findFirst().get().getId());
            }
        });
        List<Genre> bookGenres = book.getGenres();
        bookGenres.stream().forEach(genre -> {
            if(!isGanrePresent(genre)) {
                genreService.saveGenre(genre);
                genres.add(genre);
            } else {
                bookGenres.get(bookGenres.indexOf(genre)).setId(genres.stream().filter(g -> g.getName().equals(genre.getName())).findFirst().get().getId());
            }
        });
    }
}

$(document).ready(function () {
    $(".run").click(function () {
        $.ajax({
            url: "/crawlers/" + this.value + "/run",
            type: "POST",
            success: function() {

            }
        });
        getProgress(this.value);
    });
    $(".stop").click(function () {
        $.ajax({
            url: "/crawlers/" + this.value + "/stop",
            type: "POST",
            success: function() {

            }
        });
    });
    $(".pause").click(function () {
        $.ajax({
            url: "/crawlers/" + this.value + "/pause",
            type: "POST",
            success: function() {

            }
        });
    });


});

function getProgress(opdsId) {
    setInterval(function() {
        //$(".progress").each(function () {
            //var opdsId =  $(this).attr("opds-id");
            $opdsProgressElement = $("#opds_" + opdsId);
            $.ajax({
                url: "/link/progress",
                type: "GET",
                async: false,
                data: {opdsId : opdsId},
                success: function(data) {
                    $opdsProgressElement.text(data)
                }
            });
       // });

    }, 4000);
}
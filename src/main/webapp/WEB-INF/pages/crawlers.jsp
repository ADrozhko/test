<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="include/header.jsp" />
<body>
<div class="content">
    <table class="opds">
        <tr>
            <td>Name:</td>
            <td>Url:</td>
            <td>Actions:</td>
            <td>Url crawling progress:</td>
        </tr>
        <c:forEach items="${opdsList}" var="opds">
            <tr>
                <td>${opds.getName()}</td>
                <td>${opds.getUrl()}</td>
                <td>
                    <button class="run" value="${opds.id}">Run</button>
                    <button class="pause" value="${opds.id}">Pause</button>
                    <button class="stop" value="${opds.id}">Stop</button>
                </td>
                <td class="progress" id="opds_${opds.id}" opds-id="${opds.id}">
                </td>
            </tr>
        </c:forEach>
    </table>

</div>
</body>
<jsp:include page="include/footer.jsp" />